var fetch = require('../../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
var moment = require('moment');

var scrap = async function (calendarId, openAgendaId) {
	var calendar = [];
	try {
		var gouv = [];
		var gouvSearch = {};
		var filePath = path.join(process.cwd(), 'data', 'gouv.json');
		const gouvData = await fs.promises.readFile(filePath);
		gouv = JSON.parse(gouvData);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}

		var now = moment();
		if (now.day() == 0) {
			var premierJourDeLaSemaine = now.subtract(1, 'weeks').day(1);
		} else {
			var premierJourDeLaSemaine = now.day(1);
		}
		var weekKey = premierJourDeLaSemaine.format("DDMMYYYY");

		var offset = 0;
		var limit = 300;
		var id = 0;
		do {
			const res = await fetch.nSave('https://openagenda.com/agendas/' + openAgendaId + '/events.json?limit=' + limit + '&offset=' + offset + '&oaq[passed]=1&key=a3d84350f4b44ce9876bae4c6f87be33', calendarId + '-' + weekKey + '-' + offset + '.json');

			var data = JSON.parse(await res);

			for (e in data.events) {
				calendar.push({
					id: calendarId + "-" + id,
					calendarId: calendarId,
					title: data.events[e]['title']['fr'],
					category: 'time',
					start: data.events[e]['timings'][0]['start'],
					end: data.events[e]['timings'][0]['end'],
					location: data.events[e]['location']['name'],
					attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
				});
				id++;
			}
			offset += limit;
		} while (data.total > data.offset + data.limit)
		//console.log(calendar);
		var filePath = path.join(process.cwd(), 'data', 'agendas', calendarId + '.json');
		fs.writeFile(filePath, JSON.stringify(Object.values(calendar), null, 2), function (err) {
			if (err) {
				return console.log(calendarId + " " + err);
			} else {
				if (global.env == "dev") {
					console.log("[" + calendarId + "] : " + calendar.length + " events saved !");
				}
			}

		});

	} catch (err) {
		console.error(err);
	}

}

exports.scrap = scrap;
