var fetch = require('../../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
var moment = require('moment');

var scrap = async function (calendarId, dataLink) {
	var calendar = [];
	try {
		var gouv = [];
		var gouvSearch = {};
		var filePath = path.join(process.cwd(), 'data', 'gouv.json');
		const gouvData = await fs.promises.readFile(filePath);
		gouv = JSON.parse(gouvData);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}

		var now = moment();
		if(now.day() == 0){
			var premierJourDeLaSemaine = now.subtract(1, 'weeks').day(1);
		} else {
			var premierJourDeLaSemaine = now.day(1);
		}
		var weekKey = premierJourDeLaSemaine.format("DDMMYYYY");

		const res = await fetch.nSave(dataLink, calendarId+'-'+weekKey+'.json');

		var data = JSON.parse(await res);

		for (d in data) {
			calendar.push({
				id: data[d]['fields']['uid'],
				calendarId: calendarId,
				title: data[d]['fields']['description'],
				category: 'time',
				start: data[d]['fields']['dtstart'],
				end: data[d]['fields']['dtend'],
				location: '',
				attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
			});
		}
		//console.log(calendar);

		var filePath = path.join(process.cwd(), 'data', 'agendas', calendarId + '.json');
		fs.writeFile(filePath, JSON.stringify(Object.values(calendar), null, 2), function (err) {
			if (err) {
				return console.log(calendarId + " " + err);
			} else {
				if (global.env == "dev") {
					console.log("[" + calendarId + "] : " + calendar.length + " events saved !");
				}
			}

		});

	} catch (err) {
		console.error(err);
	}

}

exports.scrap = scrap;
