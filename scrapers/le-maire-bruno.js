var openAgendaScrapper = require('./common/scrap-OpenAgenda');

var calendarId = 'le-maire-bruno';
var openAgendaId = '41488194';

var scrap = function () {
	return openAgendaScrapper.scrap(calendarId, openAgendaId);
}

exports.scrap = scrap;
