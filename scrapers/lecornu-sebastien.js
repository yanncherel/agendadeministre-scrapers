var minCohesionTerritoiresScrapper = require('./common/min-cohesion-territoires');

var calendarId = 'lecornu-sebastien';
var minId = 'sebastien-lecornu';

var scrap = function () {
	return minCohesionTerritoiresScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
