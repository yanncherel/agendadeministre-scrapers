var openAgendaScrapper = require('./common/scrap-OpenAgenda');

var calendarId = 'o-cedric';
var openAgendaId = '3492148';

var scrap = function () {
	return openAgendaScrapper.scrap(calendarId, openAgendaId);
}

exports.scrap = scrap;
