var openAgendaScrapper = require('./common/scrap-OpenAgenda');

var calendarId = 'pannier-runacher-agnes';
var openAgendaId = '31602919';

var scrap = function () {
	return openAgendaScrapper.scrap(calendarId, openAgendaId);
}

exports.scrap = scrap;
