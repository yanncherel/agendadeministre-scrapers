var minEcoloSolidaireScrapper = require('./common/min-ecologique-solidaire');

var calendarId = 'wargon-emmanuelle';
var minId = 'demmanuelle-wargon';

var scrap = function () {
	return minEcoloSolidaireScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
