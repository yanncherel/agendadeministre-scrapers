#!/bin/env node

/* eslint-disable no-continue, no-restricted-syntax */
const fs = require('fs').promises;
const path = require('path');
const mkdirp = require('mkdirp');

(async () => {
  const gouvBuffer = await fs.readFile(path.join(__dirname, '../data/gouv.json'));
  const gouvJson = gouvBuffer.toString();
  const gouv = JSON.parse(gouvJson);

  await mkdirp(path.join(process.cwd(), 'data', 'agendas'));
  await mkdirp(path.join(process.cwd(), 'logs'));
  await mkdirp(path.join(process.cwd(), 'archives'));

  for await (const { id } of gouv) {
    const scraperPath = `../scrapers/${id}.js`;
    let scraper;

    let exists = false;
    try {
      await fs.stat(path.join(__dirname, scraperPath));
    } catch (e) {
      console.error(`No scraper for ${id}`);
      continue;
    }

    try {
      scraper = require(scraperPath);
    } catch (e) {
      console.error(`Error requiring scrapper for ${id}`);
      console.error(e);
      continue;
    }

    try {
      console.log(`Scraping data for ${id}`)
      await scraper.scrap();
    } catch (e) {
      console.log(`Error scraping data for ${id}`);
      console.error(e);
      continue;
    }
  }
})();
